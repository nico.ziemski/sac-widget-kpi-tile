(function()  {
    let template = document.createElement("template");
    template.innerHTML = `
		<img class="img" src="https://gitlab.com/nico.ziemski/sac-widget-kpi-tile/raw/808cae9cf075d88cce3cc72b0ac34ff8f54a67c8/Logo_320x132(public).png" alt="NextLytics AG Logo">
		<form id="form">
			<fieldset>
				<legend>Builder XYZ Properties</legend>
				<table>
					<tr>
						<td>Set your builder properties here</td>
					</tr>
				</table>
			</fieldset>
		</form>
		<style>
			:host {
				background-color: #00929f;
				color: white;
				display: block;
				padding: 1em 1em 1em 1em;
			}
			.img {
				display: block;
  				margin-left: auto;
  				margin-right: auto;
  				width: 50%;
			}
		</style>
    `;

    class TextSDKBuilder extends HTMLElement {
        constructor(){
            super();
            this._shadowRoot = this.attachShadow({mode: "open"});
            this._shadowRoot.appendChild(template.content.cloneNode(true));
        }
    }

    customElements.define("text-sdk-builder", TextSDKBuilder); 
})();